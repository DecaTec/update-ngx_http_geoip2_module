[![MIT License](https://img.shields.io/badge/license-MIT-green)](https://codeberg.org/DecaTec/update-ngx_http_geoip2_module/src/branch/main/LICENSE)
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=jr%40decatec%2ede&lc=US&item_name=update%20nginx%20http%20geoip2%20module&no_note=1&no_shipping=1&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted)

# update-ngx_http_geoip2_module

Script for automated update of the nginx module ngx_http_geoip2_module as described here: https://decatec.de/home-server/nginx-besucher-mittels-geoip2-nach-laendern-blockieren-geoblocking/