#!/bin/bash

cd /tmp
rm -rf nginx*
rm -rf ngx_http_geoip2_module

git clone https://github.com/leev/ngx_http_geoip2_module.git
NGINX_VERSION=$(nginx -v 2>&1|cut -d"/" -f 2)
wget http://nginx.org/download/nginx-$NGINX_VERSION.tar.gz
tar zxvf nginx-$NGINX_VERSION.tar.gz
cd nginx-$NGINX_VERSION

./configure --with-compat --add-dynamic-module=/tmp/ngx_http_geoip2_module
make modules
cp objs/ngx_http_geoip2_module.so /etc/nginx/modules/ngx_http_geoip2_module.so

/usr/sbin/service nginx restart